# YARPP Thumbnails SEO Optimized Template

This repository contains a custom template for the Yet Another Related Posts Plugin (YARPP) in WordPress. It allows you to display related blog posts as thumbnails without affecting the HTML structure of your blog posts. The template is designed to be SEO optimized and requires a theme that supports post thumbnails.

## Instructions

To use this template, follow these steps:

1. Install the YARPP plugin in your WordPress site.
2. Download the `yarpp-template-seo-optimized-thumbnails.php` file from this repository.
3. Access your website's root folder either through cPanel or FTP.
4. Upload the `yarpp-template-seo-optimized-thumbnails.php` file to the root folder of your current WordPress theme.
5. In your WordPress dashboard, go to **Settings** > **YARPP**.
6. In the **Automatic Display** section, select "Custom" as the visualization option.
7. From the **Template file** dropdown, select "Thumbnails SEO Optimized" option.
8. Save your settings.

## Modifying the CSS

If you want to customize the CSS styles of the related posts section, you can edit the `<style>` section of the `yarpp-template-seo-optimized-thumbnails.php` file. Feel free to make changes to suit your needs.

