<?php
/*
YARPP Template: Thumbnails SEO Optimized
Description: This template returns the related posts as thumbnails in an ordered list. Requires a theme which supports post thumbnails.
Author: snellrojas.com
*/
?>


<?php
/* Pick Thumbnail */
global $_wp_additional_image_sizes;
if ( isset( $_wp_additional_image_sizes['yarpp-thumbnail'] ) ) {
    $dimensions['size'] = 'yarpp-thumbnail-custom';
} else {
    $dimensions['size'] = 'medium'; // default
}
?>

<style>
	.yarpp-thumbnail-custom{
		width: 97% !important;
		height: 75% !important;
	}
    .related-posts {
        display: grid;
        grid-template-columns: repeat(3, 1fr); /* Adjust the number of columns as needed */
        grid-gap: 20px; /* Adjust the gap between grid items as needed */
    }

    .related-posts li {
        list-style-type: none;
    }

    .related-posts li img {
        width: 100%;
        height: auto;
        object-fit: cover;
        aspect-ratio: 300/200; /* Set the desired aspect ratio */
    }
	.yarpp-custom-title{
		margin: 7px;
    	margin-top: 0px;
    	width: 19.5rem;
		text-align: center !important;
		font-size: 95%!important;
		color: #54595F;
	}
	.yarpp-custom-title:hover{
		margin: 7px;
    	margin-top: 0px;
    	width: 19.5rem;
		text-align: center !important;
		font-size: 100%!important;
		color: #b42f2c;
	}
	.related-posts-title{
		font-size: 125%;
		font-weight: 700;
		padding: 0 0 5px;
		text-transform: capitalize;
	}
</style>

<p class="related-posts-title">Publicaciones relacionadas:</p>
<?php if ( have_posts() ) : ?>
<ul class="related-posts">
    <?php
    while ( have_posts() ) :
        the_post();
        ?>
        <?php if ( has_post_thumbnail() ) : ?>
        <li>
            <a href="<?php the_permalink(); ?>" rel="bookmark norewrite" title="<?php the_title_attribute(); ?>">
                <?php the_post_thumbnail( $dimensions['size'], array( 'data-pin-nopin' => 'true' ) ); ?>
                <p class="yarpp-custom-title"><?php the_title(); ?></p> <!-- Display the post title -->
            </a>
        </li>
        <?php endif; ?>
    <?php endwhile; ?>
</ul>

<?php else : ?>
<p>No hay entradas relacionadas.</p>
<?php endif; ?>
